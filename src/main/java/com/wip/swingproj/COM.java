/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.swingproj;

import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author WIP
 */
public class COM {

    private int hand; //rock=0, scissors=1, paper=2
    private int playerHand; //rock=0, scissors=1, paper=2
    private int win, lose, draw;
    private int status;

    public COM() {

    }

    private int Random() {
        return ThreadLocalRandom.current().nextInt(0, 3);
    }

    public int jankenpon(int playerHand) { // win=1, draw=0, lose=-1
        this.playerHand = playerHand;
        this.hand = Random();
        if (this.playerHand == this.hand) {
            draw++;
            status = 0;
            return 0;
        }
        if (this.playerHand == 0 && this.hand == 1
                || this.playerHand == 1 && this.hand == 2
                || this.playerHand == 2 && this.hand == 0) {
            win++;
            status = 1;
            return 1;
        }

        lose++;
        status = -1;
        return -1;
    }

    public int getHand() {
        return hand;
    }

    public int getPlayerHand() {
        return playerHand;
    }

    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }

    public int getStatus() {
        return status;
    }

}
